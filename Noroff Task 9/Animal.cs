﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_9
{
    class Animal
    {
        private string type;
        private string name;
        private int age;

        public Animal(string type, string name, int age)
        {
            this.type = type;
            this.name = name;
            this.age = age;
        }

        //What's the point of having a pet if you can't pet it, right?
        public void PetAnimal()
        {
            Console.WriteLine($"You petted {name} the {type}. Whoopidoo.");
        }

        //Prints the stored information about the animal
        public void PrintInfo()
        {
            Console.WriteLine($"{name} is a {age} year old {type}");
        }

        public string Type {
            get { return type; }
            set { type = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

    }
}
