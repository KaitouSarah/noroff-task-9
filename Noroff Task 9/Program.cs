﻿using System;
using System.Collections.Generic;

namespace Noroff_Task_9
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animalList = GenerateAnimalList();
            PrintAndPetAnimals(animalList);

        }

        //Creates a hard coded list of 3 animals.
        private static List<Animal> GenerateAnimalList()
        {
            Animal bambi = new Animal("Dog", "Bambi", 4);
            Animal fido = new Animal("Cat", "Fido", 5);
            Animal rex = new Animal("Hamster", "Rex", 1);

            List<Animal> animalList = new List<Animal>();
            animalList.Add(bambi);
            animalList.Add(fido);
            animalList.Add(rex);

            return animalList;
        }

        //Prints the list of animals and pets them.
        private static void PrintAndPetAnimals(List<Animal> animalList)
        {
            foreach (Animal animal in animalList)
            {
                animal.PrintInfo();
                animal.PetAnimal();
            }
        }
    }
}
